#ifndef ASMARTPTR_HPP_
# define ASMARTPTR_HPP_
# include "RetroSTLCommon.hpp"

namespace unnamed {
    namespace retrostl {
        template <typename T>
        class ASmartPtr {
            public:
                ASmartPtr() {}

                ASmartPtr(const ASmartPtr &other) {}

                virtual ~ASmartPtr() {}

                void reset(T *ptr = nullptr) {
                    unset();
                    if (ptr != nullptr) {
                        set(ptr);
                    }
                }

                void swap(ASmartPtr &other) {
                    T* tmp = other.m_value;
                    other.set(m_value);
                    set(tmp);
                }

                T *get() const {
                    return m_value;
                }

                T &operator*() const {
                    if (m_value == nullptr) {
                        // implement exception or default value
                    }
                    return *m_value;
                }

                T *operator->() const {
                    return get();
                }

                operator bool() const {
                    return m_value != nullptr;
                }

            protected:
                virtual void set(T *ptr) = 0;

                virtual void unset() = 0;

                void copy(const ASmartPtr &other) {
                    if (m_value != nullptr) {
                        unset();
                    }
                    set(other.get());
                }

            private:
                ASmartPtr &operator=(const ASmartPtr &other) {}

            protected:
                T* m_value;
        };
    }
}

#endif // ASMARTPTR_HPP_