#ifndef RETROSTLCOMMON_HPP_
# define RETROSTLCOMMON_HPP_
# include <cstdlib>
# if defined(__cplusplus) && __cplusplus < 201103L
// Hack to use nullptr keyword with pre-C++11 compilers
#  define nullptr NULL
# endif // defined(__cplusplus) && __cplusplus < 201103L
#endif // RETROSTLCOMMON_HPP_