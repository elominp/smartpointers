#ifndef ASHAREDPTR_HPP_
# define ASHAREDPTR_HPP_
# include "ASmartPtr.hpp"
# include <map>

namespace unnamed {
    namespace retrostl {
        template <typename T>
        class ASharedPtr : public ASmartPtr<T> {
            public:
                ASharedPtr():ASmartPtr<T>() {}

                ASharedPtr(const ASharedPtr &other):ASmartPtr<T>(other) {}

                virtual ~ASharedPtr() {}

                long use_count() const {
                    return m_pointers[ASmartPtr<T>::m_value];
                }

                bool unique() const {
                    return use_count() == 1;
                }

            protected:
                virtual void set(T *ptr) = 0;

                virtual void unset() = 0;

            private:
                ASharedPtr &operator=(const ASharedPtr &other) {}

            protected:
                static std::map<void *, size_t> m_pointers;
        };

        template <typename T>
        std::map<void *, size_t> ASharedPtr<T>::m_pointers;
    }
}

# endif // ASHAREDPTR_HPP_